# Black Friday Challenge - Shipping

## To view the code for this project, please click on the links below:
- Frontend - https://gitlab.io.thehut.local/Accelerator18/logistics-frontend.git
- Backend - https://gitlab.io.thehut.local/Accelerator18/logistics-dashboard.git

### Priorities discussed in first meeting:
* To produce a **clean** product which is very **visual** and **easy to understand** on first glance
* Product should be **highly functioning** and **easily modifiable**
* Use **pair programming** to increase quality and understanding of work across the team
* Start simple: create a high quality product first, then think about adding features once we have a watertight base to work from
* Decide on a clear MVP (minimum viable product)
* **Meet daily** at 11am for a stand-up to discuss progress, struggles and plans


### Initial Product:
We are aiming to produce a live map that updates in real time to show where THG products are being shipped to over the Black Friday period. We will use **colour coded dots** to represent which warehouse the products are being shipped from, and as a starting case we will have **one dot per country** and choose the colour based on where the *majority* of shipments to that country will come from.

We will represent the amount of orders being shipped to a country at any one time by the size of the dot being displayed on said country: **the larger the dot the larger the number of shipments being processed**. In order to avoid noise and get a more accurate view of the bigger picture, we will display our data according to a **moving window** of a relatively large time period, say half an hour (to be confirmed later when we have more information about the data we will be receiving).


### Potential Extensions:
* Incorporate a representation of what **proportion of shipments** are coming from which warehouse: make each dot into a **pie chart** to represent this.
* Add a way to display **cumulative data** over the course of black friday weekend - potentially using numbers ticking up or a growing bar chart. Decide whether to give this its own screen or add to the initial product screen.
* Store and look back over **last 24 hours** of live updates - potentially have a feature where you can view the past 24 hours sped up over a much shorter period of time.
* Compare number of orders being received with number of orders being shipped, watch in real time the **growth of the back log** of shipments to be made (consider using numbers and/or bar charts to represent this data).
* Think about adding these or other metrics to our initial product as facts floating on the ocean - discuss whether it is better to have these on separate pages.


### Jobs:
`Front End:`

* Learn how to change size of dots
* Positioning dots: how and where
* Get the map to use as our base

`Back End:`

* Figure out how we will receive the data and make it usable
* Create a database to store the relevant data and make it easily accessible
* Figure out how to implement a moving window of time

`Other:`

* Think about America - are we going to have it on the map or not? If not, how will we represent items being shipped to America?
